package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

 
public class App 
{
    public static void main( String[] args )
    {
    	Connection conn = null;
    	 
    	Statement stmt = null;
    	
		try {
			// JDBC CONNECTION
		    conn = DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");
		    stmt = conn.createStatement();
		     
			//SQL
			String sql  = "SELECT * from categories";
			
			// execute the query, and get a java ResultSet
			ResultSet rs = stmt.executeQuery(sql);
	       	        
	        while(rs.next())
	        {		        
	        	System.out.println(rs.getInt("idCategory") +  " " + rs.getString("name") );
	        }
	        
	            
		} catch (Exception ex) {
		   
		    System.out.println("Exception: " + ex.getMessage());
 
		}		
			
    }
}
